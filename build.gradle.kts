import org.jetbrains.intellij.tasks.PatchPluginXmlTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.intellij") version "0.4.15"
    id("org.jetbrains.kotlin.jvm") version "1.3.61"
}

group = "to.bri.intellij"
version = "0.1.3"

repositories {
    mavenCentral()
}

// See https://github.com/JetBrains/gradle-intellij-plugin/
intellij {
    version = "2019.3"
    pluginName = "tasks-clubhouse"
    setPlugins("tasks")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = "1.8"
        languageVersion = "1.3"
        apiVersion = "1.3"
    }
}

tasks.withType<PatchPluginXmlTask> {
    changeNotes(file("docs/change-notes.html").readText())
    sinceBuild("193")
    untilBuild("201.*")
}
