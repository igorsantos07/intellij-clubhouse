package to.bri.intellij.clubhouse

import to.bri.intellij.clubhouse.api.Comment
import java.util.*

class ClubhouseComment(private val comment: Comment) : com.intellij.tasks.Comment() {

    override fun getText(): String {
        return comment.text
    }

    override fun getAuthor(): String? {
        return null
    }

    override fun getDate(): Date? {
        return comment.createdAt
    }

}
