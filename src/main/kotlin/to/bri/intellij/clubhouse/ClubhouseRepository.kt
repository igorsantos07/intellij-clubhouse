package to.bri.intellij.clubhouse

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.intellij.tasks.Task
import com.intellij.tasks.impl.BaseRepository
import com.intellij.tasks.impl.httpclient.NewBaseRepositoryImpl
import com.intellij.util.xmlb.annotations.Tag
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.slf4j.LoggerFactory
import to.bri.intellij.clubhouse.api.MemberInfo
import to.bri.intellij.clubhouse.api.StorySearchResults
import java.util.*
import kotlin.math.min

@Tag("Clubhouse")
class ClubhouseRepository : NewBaseRepositoryImpl {

    private val logger = LoggerFactory.getLogger(ClubhouseRepository::class.java)
    private val objectMapper = buildObjectMapper()
    private var owner: MemberInfo? = null

    @Suppress("unused")
    constructor() : super()

    constructor(type: ClubhouseRepositoryType?) : super(type)
    constructor(other: ClubhouseRepository?) : super(other)

    override fun clone(): BaseRepository {
        return ClubhouseRepository(this)
    }

    override fun getUrl(): String {
        return "https://api.clubhouse.io"
    }

    override fun getPresentableName(): String {
        return "clubhouse.io"
    }

    override fun createCancellableConnection(): CancellableConnection? {
        val uri = URIBuilder(getRestApiUrl("member"))
            .addParameter("token", password)
            .build()

        return HttpTestConnection(HttpGet(uri))
    }

    override fun getRestApiPathPrefix(): String {
        return "/api/v3"
    }

    @Throws(Exception::class)
    override fun getIssues(search: String?, offset: Int, limit: Int, withClosed: Boolean): Array<Task> {
        val tokens: MutableList<String> = ArrayList()
        if (!withClosed) tokens.add("!is:done")

        if (search != null && search.isNotBlank()) {
            tokens.add(search.trim())
        } else {
            getDefaultQuery()?.let { tokens.add(it) }
        }

        val query = java.lang.String.join(" ", tokens)

        val searchUri = URIBuilder(getRestApiUrl("search", "stories"))
            .addParameter("query", query)
            .addParameter("token", password)
            .addParameter("page_size", min(25, limit).toString())
            .build()

        val searchMethod = HttpGet(searchUri)

        logger.debug("Requesting stories from clubhouse with query {}", query)
        return httpClient.execute(searchMethod, this::parseStories)
    }

    private fun getDefaultQuery(): String? {
        return getOwner()?.mentionName
    }

    private fun getOwner(): MemberInfo? {
        if (owner == null) owner = fetchOwner()
        return owner
    }

    private fun fetchOwner(): MemberInfo? {
        val uri = URIBuilder(getRestApiUrl("member"))
            .addParameter("token", myPassword)
            .build()

        val method = HttpGet(uri)

        return try {
            httpClient.execute(method, this::parseMemberInfo)
        } catch (e: Exception) {
            logger.error("Failed to fetch current member info", e)
            null
        }
    }

    private fun parseMemberInfo(response: HttpResponse): MemberInfo? {
        return if (response.statusLine.statusCode == 200) {
            val input = response.entity.content
            objectMapper.readValue(input, MemberInfo::class.java)
        } else {
            logger.error("Failed to parse current member info: {}", response.statusLine)
            null
        }
    }

    private fun parseStories(response: HttpResponse): Array<Task> {
        return if (response.statusLine.statusCode == 200) {
            val input = response.entity.content
            val (_, stories) = objectMapper.readValue(input, StorySearchResults::class.java)
            stories.map { story -> ClubhouseTask(story, this) }.toTypedArray()
        } else {
            logger.error("Failed to parse stories: {}", response.statusLine)
            arrayOf()
        }
    }

    @Throws(Exception::class)
    override fun findTask(id: String): Task? {
        return null
    }

    private fun buildObjectMapper(): ObjectMapper {
        val objectMapper = ObjectMapper()
        objectMapper.registerModule(KotlinModule())
        return objectMapper
    }

}
